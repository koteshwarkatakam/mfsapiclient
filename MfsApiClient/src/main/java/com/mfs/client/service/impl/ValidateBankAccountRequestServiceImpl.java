package com.mfs.client.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.stereotype.Service;

import com.mfs.client.dto.Code;
import com.mfs.client.dto.ReturnDto;
import com.mfs.client.dto.ValidateBankAccountRequestDto;
import com.mfs.client.dto.ValidateBankAccountResponseDto;
import com.mfs.client.exception.CommonException;
import com.mfs.client.service.ValidateBankAccountRequestService;
import com.mfs.client.util.CallService;
import com.mfs.client.util.CommonConstant;
import com.mfs.client.util.HttpsConnectionRequest;
import com.mfs.client.util.HttpsConnectionResponse;
import com.mfs.client.util.ResponseCodes;

@Service("ValidateBankAccountRequestService")
public class ValidateBankAccountRequestServiceImpl implements ValidateBankAccountRequestService {

	private static final Logger LOGGER = LogManager.getLogger(ValidateBankAccountRequestServiceImpl.class);

	public ValidateBankAccountResponseDto validateBankAccountRequest(ValidateBankAccountRequestDto request) {

		ValidateBankAccountResponseDto response = new ValidateBankAccountResponseDto();
		String soapRequest = "";
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		Map<String, String> header = new HashMap<String, String>();
		HttpsConnectionResponse httpsConResult = new HttpsConnectionResponse();
		String serviceResponse = "";
		Code code = new Code();
		ReturnDto returnDto = new ReturnDto();

		try {

			// set soap request
			MessageFactory messageFactory;
			messageFactory = MessageFactory.newInstance();
			SOAPMessage soapMessage = messageFactory.createMessage();
			SOAPPart soapPart = soapMessage.getSOAPPart();
			SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
			soapEnvelope.addNamespaceDeclaration("ws", "http://ws.mfsafrica.com");
			soapEnvelope.addNamespaceDeclaration("xsd", "http://mfs/xsd");

			SOAPBody localSOAPBody = soapEnvelope.getBody();
			SOAPElement localSOAPElement1 = localSOAPBody.addChildElement("validate_bank_account", "ws");

			SOAPElement localSOAPElement2 = localSOAPElement1.addChildElement("login", "ws");

			SOAPElement localSOAPElement3 = localSOAPElement2.addChildElement("corporate_code", "xsd");
			localSOAPElement3.addTextNode(request.getLogin().getCorporate_code());

			SOAPElement localSOAPElement4 = localSOAPElement2.addChildElement("password", "xsd");
			localSOAPElement4.addTextNode("");

			SOAPElement localSOAPElement5 = localSOAPElement1.addChildElement("payee", "ws");

			SOAPElement localSOAPElement6 = localSOAPElement5.addChildElement("msisdn", "xsd");
			localSOAPElement6.addTextNode(request.getPayee().getMsisdn());

			SOAPElement localSOAPElement7 = localSOAPElement1.addChildElement("account", "ws");

			SOAPElement localSOAPElement8 = localSOAPElement7.addChildElement("account_number", "xsd");
			localSOAPElement8.addTextNode(request.getAccount().getAccount_number());

			SOAPElement localSOAPElement9 = localSOAPElement7.addChildElement("mfs_bank_code", "xsd");
			localSOAPElement9.addTextNode(request.getAccount().getMfs_bank_code());

			SOAPElement localSOAPElement10 = localSOAPElement1.addChildElement("to_country", "ws");
			localSOAPElement10.addTextNode(request.getTo_country());

			soapRequest = getXmlFromSOAPMessage(soapMessage);

			LOGGER.info(
					"==>In MFSApiClient ValidateBankAccountRequestServiceImpl of validateBankAccountRequest function mfs account_request for msisdn "
							+ request.getAccount().getAccount_number() + "      " + soapRequest);

			soapRequest = soapRequest.replace("<xsd:password/>",
					"<xsd:password>" + request.getLogin().getPassword() + "</xsd:password>");

			connectionRequest.setServiceUrl(request.getWsdl());
			header.put(CommonConstant.CONTENT_TYPE, CommonConstant.TEXT_XML);
			// checking authentication is required or not for calling mfs
			if (request.getxApiKey() != null) {

				header.put(CommonConstant.X_API_KEY, request.getxApiKey());

			}
			connectionRequest.setHeaders(header);

			// call to mfs api
			httpsConResult = CallService.getResponseFromService(connectionRequest, soapRequest);

			if (httpsConResult != null) {

				serviceResponse = httpsConResult.getResponseData();

			}
			LOGGER.info(
					"==>In MFSApiClient ValidateBankAccountRequestServiceImpl of validateBankAccountRequest function mfs validate_bank_account response for account number  "
							+ request.getAccount().getAccount_number() + "     " + serviceResponse);

			if (httpsConResult == null) {

				response.setCode(ResponseCodes.ER215.getCode());
				response.setMessage(ResponseCodes.ER215.getMessage());

			} else {

				if (serviceResponse != null) {

					JSONObject obj = XML.toJSONObject(serviceResponse);
					String jso1 = obj.getJSONObject("soapenv:Envelope").getJSONObject("soapenv:Body")
							.getJSONObject("ns:validate_bank_accountResponse").getJSONObject("ns:return").toString();

					JSONObject obj2 = new JSONObject(jso1);

					String accountHolderName = String.valueOf(obj2.get("ax21:account_holder_name"));
					String accountNumber = String.valueOf(obj2.get("ax21:account_number"));
					String mfsBankCode = String.valueOf(obj2.get("ax21:mfs_bank_code"));
					String statusCode = (String) obj2.getJSONObject("ax21:status").get("ax21:status_code");

					returnDto.setAccount_holder_name(accountHolderName);
					returnDto.setAccount_number(accountNumber);
					returnDto.setMfs_bank_code(mfsBankCode);
					code.setStatus_code(statusCode);

					returnDto.setStatus(code);

					response.setReturnDto(returnDto);

				} else {

					response.setCode(String.valueOf(httpsConResult.getRespCode()));
					response.setMessage(httpsConResult.getTxMessage());

				}
			}

		} catch (CommonException ce) {
			LOGGER.error(
					"==>CommonException on MFSApiClient of ValidateBankAccountRequestServiceImpl in validateBankAccountRequest function ",
					ce);
			response.setCode(ce.getStatus().getStatusCode());
			response.setMessage(ce.getStatus().getStatusMessage());

		} catch (Exception e) {
			LOGGER.error(
					"==>Exception on MFSApiClient of ValidateBankAccountRequestServiceImpl in validateBankAccountRequest function",
					e);
			response.setCode(ResponseCodes.ER201.getCode());
			response.setMessage(ResponseCodes.ER201.getMessage());
		}

		return response;
	}

	static String getXmlFromSOAPMessage(SOAPMessage paramSOAPMessage) throws SOAPException, IOException {
		ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
		paramSOAPMessage.writeTo(localByteArrayOutputStream);
		return new String(localByteArrayOutputStream.toByteArray());
	}

}
