package com.mfs.client.service;

import com.mfs.client.dto.BankRemitLogRequestDto;
import com.mfs.client.dto.BankRemitLogResponseDto;

public interface BankRemitLogService {

	public BankRemitLogResponseDto bankRemitLog(BankRemitLogRequestDto request);
	
	
}
