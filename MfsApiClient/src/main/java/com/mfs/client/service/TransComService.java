package com.mfs.client.service;

import com.mfs.client.dto.TransComRequestDto;
import com.mfs.client.dto.TransComResponseDto;

public interface TransComService {
	public TransComResponseDto transComm(TransComRequestDto request) throws Exception;
}
