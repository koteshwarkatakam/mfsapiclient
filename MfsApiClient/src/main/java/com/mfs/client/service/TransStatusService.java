package com.mfs.client.service;

import com.mfs.client.dto.TransStatusRequestDto;
import com.mfs.client.dto.TransStatusResponseDto;

public interface TransStatusService {
	
	public TransStatusResponseDto getTransStatus(TransStatusRequestDto request); 

}
