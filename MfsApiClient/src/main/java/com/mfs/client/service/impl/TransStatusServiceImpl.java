package com.mfs.client.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.stereotype.Service;

import com.mfs.client.dto.Code;
import com.mfs.client.dto.TransStatusRequestDto;
import com.mfs.client.dto.TransStatusResponseDto;
import com.mfs.client.exception.CommonException;
import com.mfs.client.service.TransStatusService;
import com.mfs.client.util.CallService;
import com.mfs.client.util.CommonConstant;
import com.mfs.client.util.HttpsConnectionRequest;
import com.mfs.client.util.HttpsConnectionResponse;
import com.mfs.client.util.ResponseCodes;

@Service("TransStatusService")
public class TransStatusServiceImpl implements TransStatusService {

	private static final Logger LOGGER = LogManager.getLogger(TransStatusServiceImpl.class);

	public TransStatusResponseDto getTransStatus(TransStatusRequestDto request) {

		String serviceResponse = null;
		Map<String, String> headerMap = new HashMap<String, String>();
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		TransStatusResponseDto response = new TransStatusResponseDto();
		HttpsConnectionResponse httpsConResult = new HttpsConnectionResponse();
		Code code = new Code();

		try {

			// creating soap request
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage soapMessage = messageFactory.createMessage();
			SOAPPart soapPart = soapMessage.getSOAPPart();

			// creating soap envelop
			SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
			soapEnvelope.addNamespaceDeclaration("ws", "http://ws.mfsafrica.com");
			soapEnvelope.addNamespaceDeclaration("xsd", "http://mfs/xsd");

			// creating soap header
			SOAPHeader soapHeader = soapEnvelope.getHeader();

			// creating soap body & its parameters
			SOAPBody soapBody = soapEnvelope.getBody();

			SOAPElement soapElement = soapBody.addChildElement("get_trans_status", "ws");
			SOAPElement soapElement1 = soapElement.addChildElement("login", "ws");

			SOAPElement element1 = soapElement1.addChildElement("corporate_code", "xsd");
			element1.addTextNode(request.getLogin().getCorporate_code());
			SOAPElement element2 = soapElement1.addChildElement("password", "xsd");
			element2.addTextNode("");
			SOAPElement element3 = soapElement.addChildElement("trans_id", "ws");
			element3.addTextNode(request.getTrans_id());

			soapMessage.saveChanges();

			String soapRequest = getXmlFromSOAPMessage(soapMessage);

			LOGGER.info(
					"==>In MFSApiCLient TransStatusServiceImpl of getTransStatus function calling mfs trans_comm for MFSID "
							+ request.getTrans_id() + "     " + soapRequest);

			soapRequest = soapRequest.replace("<xsd:password/>",
					"<xsd:password>" + request.getLogin().getPassword() + "</xsd:password>");

			// preparing connection request
			connectionRequest.setServiceUrl(request.getWsdl());

			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.TEXT_XML);

			// checking authentication is required or not for calling mfs
			if (request.getxApiKey() != null) {

				headerMap.put(CommonConstant.X_API_KEY, request.getxApiKey());

			}
			connectionRequest.setHeaders(headerMap);

			// call to mfs api
			httpsConResult = CallService.getResponseFromService(connectionRequest, soapRequest);

			if (httpsConResult != null) {

				serviceResponse = httpsConResult.getResponseData();

			}
			LOGGER.info(
					"==>In MFSApiCLient TransStatusServiceImpl of getTransStatus function mfs trans_comm response for MFSID "
							+ request.getTrans_id() + "   " + serviceResponse);

			if (httpsConResult == null) {

				code.setStatus_code(ResponseCodes.ER215.getCode());
				response.setCode(code);
				response.setStatusMessage(ResponseCodes.ER215.getMessage());

			} else {

				if (httpsConResult.getResponseData() != null) {

					JSONObject obj = XML.toJSONObject(serviceResponse);
					String soapResponse = obj.getJSONObject("soapenv:Envelope").getJSONObject("soapenv:Body")
							.getJSONObject("ns:get_trans_statusResponse").getJSONObject("ns:return").toString();

					JSONObject obj2 = new JSONObject(soapResponse);

					String message = (String) obj2.get("ax21:message");

					String statusCode = (String) obj2.getJSONObject("ax21:code").get("ax21:status_code");

					code.setStatus_code(statusCode);
					response.setCode(code);
					response.setMessage(message);

				} else {

					code.setStatus_code(String.valueOf(httpsConResult.getRespCode()));
					response.setCode(code);

					response.setStatusMessage(httpsConResult.getTxMessage());

				}
			}

		} catch (CommonException ce) {
			LOGGER.error("==>CommonException in TransStatusServiceImpl", ce);
			code.setStatus_code(ce.getStatus().getStatusCode());
			response.setCode(code);

			response.setStatusMessage(ce.getStatus().getStatusMessage());

		} catch (Exception e) {
			LOGGER.error("==>Exception in TransStatusServiceImpl", e);
			code.setStatus_code(ResponseCodes.ER201.getCode());
			response.setCode(code);

			response.setStatusMessage(ResponseCodes.ER201.getMessage());

		}

		return response;
	}

	static String getXmlFromSOAPMessage(SOAPMessage paramSOAPMessage) throws SOAPException, IOException {
		ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
		paramSOAPMessage.writeTo(localByteArrayOutputStream);
		return new String(localByteArrayOutputStream.toByteArray());
	}
}
