package com.mfs.client.service;

import javax.xml.soap.SOAPException;

import com.mfs.client.dto.AccountRequestDto;
import com.mfs.client.dto.AccountResponseDto;

public interface AccountRequestService {
	
	public AccountResponseDto accountRequest(AccountRequestDto request) throws SOAPException, Exception;

}
