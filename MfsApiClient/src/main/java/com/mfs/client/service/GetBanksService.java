package com.mfs.client.service;

import com.mfs.client.dto.GetBanksRequestDto;
import com.mfs.client.dto.ListOfGetBanksResponseDto;

public interface GetBanksService {
	
	public ListOfGetBanksResponseDto getBanks(GetBanksRequestDto request);

}
