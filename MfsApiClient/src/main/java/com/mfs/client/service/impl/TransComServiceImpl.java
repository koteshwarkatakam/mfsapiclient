package com.mfs.client.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.stereotype.Service;

import com.mfs.client.dto.ReturnTransCom;
import com.mfs.client.dto.TransComRequestDto;
import com.mfs.client.dto.TransComResponseDto;
import com.mfs.client.service.TransComService;
import com.mfs.client.util.CallService;
import com.mfs.client.util.CommonConstant;
import com.mfs.client.util.HttpsConnectionRequest;
import com.mfs.client.util.HttpsConnectionResponse;
import com.mfs.client.util.ResponseCodes;

@Service("TransComService")
public class TransComServiceImpl implements TransComService {
	private static final Logger LOGGER = LogManager.getLogger(TransComServiceImpl.class);

	public TransComResponseDto transComm(TransComRequestDto request) throws Exception {
		String soapRequest = "";
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		Map<String, String> header = new HashMap<String, String>();
		HttpsConnectionResponse httpsConResult = new HttpsConnectionResponse();
		String serviceResponse = "";
		TransComResponseDto response = new TransComResponseDto();
		ReturnTransCom returnTrans = new ReturnTransCom();

		try {
			// set soap request
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage soapMessage = messageFactory.createMessage();
			SOAPPart soapPart = soapMessage.getSOAPPart();

			SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
			soapEnvelope.addNamespaceDeclaration("ws", "http://ws.mfsafrica.com");

			SOAPEnvelope soapEnvelope1 = soapPart.getEnvelope();
			soapEnvelope1.addNamespaceDeclaration("xsd", "http://mfs/xsd");

			SOAPHeader localSOAPHeader = soapEnvelope.getHeader();

			SOAPBody localSOAPBody = soapEnvelope.getBody();

			SOAPElement localSOAPElement1 = localSOAPBody.addChildElement("trans_com", "ws");

			SOAPElement localSOAPElement3 = localSOAPElement1.addChildElement("login", "ws");

			SOAPElement localSOAPElement6 = localSOAPElement3.addChildElement("corporate_code", "xsd");
			localSOAPElement6.addTextNode(request.getLogin().getCorporate_code());

			SOAPElement localSOAPElement7 = localSOAPElement3.addChildElement("password", "xsd");
			localSOAPElement7.addTextNode("");

			SOAPElement localSOAPElement4 = localSOAPElement1.addChildElement("trans_id", "ws");
			localSOAPElement4.addTextNode(request.getTrans_id());

			soapRequest = getXmlFromSOAPMessage(soapMessage);

			connectionRequest.setServiceUrl(request.getWsdl());
			header.put(CommonConstant.CONTENT_TYPE, CommonConstant.TEXT_XML);
			header.put(CommonConstant.X_API_KEY, request.getxApiKey());
			connectionRequest.setHeaders(header);
			LOGGER.info("==> In MFSApiClient TransComServiceImpl transComm function MFS trans_comm request for MFSID"
					+ request.getTrans_id() + "        " + soapRequest);

			soapRequest = soapRequest.replace("<xsd:password/>",
					"<xsd:password>" + request.getLogin().getPassword() + "</xsd:password>");

			// call to mfs api
			//httpsConResult = CallService.getResponseFromService(connectionRequest, soapRequest);
			serviceResponse="";
			httpsConResult = new HttpsConnectionResponse();
			httpsConResult.setRespCode(200);
			httpsConResult.setResponseData(serviceResponse);
			if (httpsConResult != null) {

				serviceResponse = httpsConResult.getResponseData();

			}
			LOGGER.info("==>In MFSApiClient TransComServiceImpl of transComm function MFS trans_comm response for MFSID"
					+ request.getTrans_id() + "        " + serviceResponse);

			if (httpsConResult == null) {

				returnTrans.setCode(ResponseCodes.ER215.getCode());
				returnTrans.setMessage(ResponseCodes.ER215.getMessage());
				response.setReturnResponse(returnTrans);

			} else {

				if (httpsConResult.getResponseData() != null) {

					JSONObject obj = XML.toJSONObject(serviceResponse);
					String jso1 = obj.getJSONObject("soapenv:Envelope").getJSONObject("soapenv:Body")
							.getJSONObject("ns:trans_comResponse").getJSONObject("ns:return").toString();

					JSONObject obj2 = new JSONObject(jso1);

					String message = (String) obj2.get("ax21:message");
					String code = (String) obj2.getJSONObject("ax21:code").get("ax21:status_code");

					returnTrans.setCode(code);

					returnTrans.setMessage(message);

					response.setReturnResponse(returnTrans);

				} else {

					returnTrans.setCode(String.valueOf(httpsConResult.getRespCode()));
					returnTrans.setMessage(httpsConResult.getTxMessage());
					response.setReturnResponse(returnTrans);

				}
			}

		} catch (Exception e) {
			LOGGER.error("==>Exception in MFSApiClient ofTransComServiceImpl", e);
			returnTrans.setCode(ResponseCodes.ER201.getCode());
			returnTrans.setMessage(ResponseCodes.ER201.getMessage());
			response.setReturnResponse(returnTrans);
			throw new Exception(e);
		}

		return response;
	}

	static String getXmlFromSOAPMessage(SOAPMessage paramSOAPMessage) throws SOAPException, IOException {
		ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
		paramSOAPMessage.writeTo(localByteArrayOutputStream);
		return new String(localByteArrayOutputStream.toByteArray());
	}

}
