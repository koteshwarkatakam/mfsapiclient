package com.mfs.client.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.stereotype.Service;

import com.mfs.client.dto.AccountRequestDto;
import com.mfs.client.dto.AccountResponseDto;
import com.mfs.client.dto.Code;
import com.mfs.client.dto.ReturnDto;
import com.mfs.client.exception.CommonException;
import com.mfs.client.service.AccountRequestService;
import com.mfs.client.util.CallService;
import com.mfs.client.util.CommonConstant;
import com.mfs.client.util.HttpsConnectionRequest;
import com.mfs.client.util.HttpsConnectionResponse;
import com.mfs.client.util.ResponseCodes;

@Service("AccountRequestService")
public class AccountRequestServiceImpl implements AccountRequestService {

	private static final Logger LOGGER = LogManager.getLogger(AccountRequestServiceImpl.class);

	public AccountResponseDto accountRequest(AccountRequestDto request) {

		AccountResponseDto response = new AccountResponseDto();
		String soapRequest = "";
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		Map<String, String> header = new HashMap<String, String>();
		HttpsConnectionResponse httpsConResult = new HttpsConnectionResponse();
		String serviceResponse = "";
		Code code = new Code();
		ReturnDto returnDto = new ReturnDto();

		try {

			// set soap request
			MessageFactory messageFactory;
			messageFactory = MessageFactory.newInstance();
			SOAPMessage soapMessage = messageFactory.createMessage();
			SOAPPart soapPart = soapMessage.getSOAPPart();
			SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
			soapEnvelope.addNamespaceDeclaration("ws", "http://ws.mfsafrica.com");
			soapEnvelope.addNamespaceDeclaration("xsd", "http://mfs/xsd");

			SOAPBody localSOAPBody = soapEnvelope.getBody();
			SOAPElement localSOAPElement1 = localSOAPBody.addChildElement("account_request", "ws");

			SOAPElement localSOAPElement2 = localSOAPElement1.addChildElement("login", "ws");

			SOAPElement localSOAPElement3 = localSOAPElement2.addChildElement("corporate_code", "xsd");
			localSOAPElement3.addTextNode(request.getLogin().getCorporate_code());

			SOAPElement localSOAPElement4 = localSOAPElement2.addChildElement("password", "xsd");
			localSOAPElement4.addTextNode("");

			SOAPElement localSOAPElement5 = localSOAPElement1.addChildElement("to_country", "ws");
			localSOAPElement5.addTextNode(request.getTo_country());

			SOAPElement localSOAPElement6 = localSOAPElement1.addChildElement("msisdn", "ws");
			localSOAPElement6.addTextNode(request.getMsisdn());

			soapRequest = getXmlFromSOAPMessage(soapMessage);
			
			LOGGER.info("==>In MFSApiClient AccountRequestServiceImpl of accountRequest function mfs account_request for msisdn "
					+request.getMsisdn()+"      "+ soapRequest);
			
			soapRequest = soapRequest.replace("<xsd:password/>", "<xsd:password>"+request.getLogin().getPassword()+"</xsd:password>");

			connectionRequest.setServiceUrl(request.getWsdl());
			header.put(CommonConstant.CONTENT_TYPE, CommonConstant.TEXT_XML);
			// checking authentication is required or not for calling mfs
			if (request.getxApiKey() != null) {

				header.put(CommonConstant.X_API_KEY, request.getxApiKey());

			}
			connectionRequest.setHeaders(header);

			// call to mfs api
			httpsConResult = CallService.getResponseFromService(connectionRequest, soapRequest);

			if (httpsConResult != null) {

				serviceResponse = httpsConResult.getResponseData();

			}
			LOGGER.info("==>In MFSApiClient AccountRequestServiceImpl of accountRequest function mfs account_request response for msisdn  "
					+request.getMsisdn()+"     "+ serviceResponse);

			if (httpsConResult == null) {

				response.setCode(ResponseCodes.ER215.getCode());
				response.setMessage(ResponseCodes.ER215.getMessage());

			} else {

				if (httpsConResult.getResponseData() != null) {

					serviceResponse = httpsConResult.getResponseData();

					JSONObject obj = XML.toJSONObject(serviceResponse);
					String jso1 = obj.getJSONObject("soapenv:Envelope").getJSONObject("soapenv:Body")
							.getJSONObject("ns:account_requestResponse").getJSONObject("ns:return").toString();

					JSONObject obj2 = new JSONObject(jso1);

					String partnerCode = String.valueOf(obj2.get("ax21:partner_code"));
					String statusCode = (String) obj2.getJSONObject("ax21:status").get("ax21:status_code");
					String msisdn = String.valueOf(obj2.get("ax21:msisdn"));

					returnDto.setPartner_code(partnerCode);
					returnDto.setMsisdn(msisdn);

					code.setStatus_code(statusCode);

					returnDto.setStatus(code);

					response.setReturnDto(returnDto);

				} else {

					response.setCode(String.valueOf(httpsConResult.getRespCode()));
					response.setMessage(httpsConResult.getTxMessage());

				}
			}

		} catch (CommonException ce) {
			LOGGER.error(
					"==>CommonException on MFSApiClient of AccountRequestServiceImpl in accountRequest function ",ce);
			response.setCode(ce.getStatus().getStatusCode());
			response.setMessage(ce.getStatus().getStatusMessage());

		} catch (Exception e) {
			LOGGER.error("==>Exception on MFSApiClient of AccountRequestServiceImpl in accountRequest function",e);
			response.setCode(ResponseCodes.ER201.getCode());
			response.setMessage(ResponseCodes.ER201.getMessage());
		}

		return response;
	}

	static String getXmlFromSOAPMessage(SOAPMessage paramSOAPMessage) throws SOAPException, IOException {
		ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
		paramSOAPMessage.writeTo(localByteArrayOutputStream);
		return new String(localByteArrayOutputStream.toByteArray());
	}
	
}
