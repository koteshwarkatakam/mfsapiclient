package com.mfs.client.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.stereotype.Service;

import com.mfs.client.dto.BankLimit;
import com.mfs.client.dto.GetBanksRequestDto;
import com.mfs.client.dto.GetBanksResponseDto;
import com.mfs.client.dto.ListOfGetBanksResponseDto;
import com.mfs.client.service.GetBanksService;
import com.mfs.client.util.CommonConstant;
import com.mfs.client.util.HttpsConnectionRequest;
import com.mfs.client.util.HttpsConnectionResponse;
import com.mfs.client.util.ResponseCodes;

@Service("GetBanksService")
public class GetBanksServiceImpl implements GetBanksService {

	private static final Logger LOGGER = LogManager.getLogger(GetBanksServiceImpl.class);

	public ListOfGetBanksResponseDto getBanks(GetBanksRequestDto request) {

		String serviceResponse = null;
		Map<String, String> headerMap = new HashMap<String, String>();
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		ListOfGetBanksResponseDto response = new ListOfGetBanksResponseDto();
		GetBanksResponseDto getBanksResponse = null;
		HttpsConnectionResponse httpsConResult = new HttpsConnectionResponse();
		BankLimit bankLimit = new BankLimit();
		List<GetBanksResponseDto> list = new ArrayList<GetBanksResponseDto>();

		try {

			// creating soap request
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage soapMessage = messageFactory.createMessage();
			SOAPPart soapPart = soapMessage.getSOAPPart();

			// creating soap envelop
			SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
			soapEnvelope.addNamespaceDeclaration("ws", "http://ws.mfsafrica.com");
			soapEnvelope.addNamespaceDeclaration("xsd", "http://mfs/xsd");

			// creating soap header
			SOAPHeader soapHeader = soapEnvelope.getHeader();

			// creating soap body & its parameters
			SOAPBody soapBody = soapEnvelope.getBody();

			SOAPElement soapElement = soapBody.addChildElement("get_banks", "ws");
			SOAPElement soapElement1 = soapElement.addChildElement("login", "ws");

			SOAPElement element1 = soapElement1.addChildElement("corporate_code", "xsd");
			element1.addTextNode(request.getLogin().getCorporate_code());
			SOAPElement element2 = soapElement1.addChildElement("password", "xsd");
			element2.addTextNode("");
			SOAPElement element3 = soapElement.addChildElement("to_country", "ws");
			element3.addTextNode(request.getToCountry());

			soapMessage.saveChanges();

			String soapRequest = getXmlFromSOAPMessage(soapMessage);

			LOGGER.info(
					"==>In MFSApiCLient GetBanksServiceImpl of getBanks function calling mfs get_banks for to_country "
							+ request.getToCountry() + "     " + soapRequest);

			soapRequest = soapRequest.replace("<xsd:password/>",
					"<xsd:password>" + request.getLogin().getPassword() + "</xsd:password>");

			// preparing connection request
			connectionRequest.setServiceUrl(request.getWsdl());

			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.TEXT_XML);

			// checking authentication is required or not for calling mfs
			if (request.getxApiKey() != null) {

				headerMap.put(CommonConstant.X_API_KEY, request.getxApiKey());

			}
			connectionRequest.setHeaders(headerMap);

			// call to mfs api
			// httpsConResult = CallService.getResponseFromService(connectionRequest,
			// soapRequest);

			httpsConResult
					.setResponseData("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
							+ "   <soapenv:Body>\n"
							+ "      <ns:get_banksResponse xmlns:ns=\"http://ws.mfsafrica.com\" xmlns:ax21=\"http://mfs/xsd\" xmlns:ax23=\"http://airtime/xsd\">\n"
							+ "         <ns:return xsi:type=\"ax21:Bank\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
							+ "            <ax21:bank_limit xsi:type=\"ax21:Limit\">\n"
							+ "               <ax21:max_daily_value>1000000.0</ax21:max_daily_value>\n"
							+ "               <ax21:max_monthly_value>7000000.0</ax21:max_monthly_value>\n"
							+ "               <ax21:max_per_tx_limit>250000.0</ax21:max_per_tx_limit>\n"
							+ "               <ax21:max_weekly_value>5000000.0</ax21:max_weekly_value>\n"
							+ "               <ax21:min_per_tx_limit>1000.0</ax21:min_per_tx_limit>\n"
							+ "            </ax21:bank_limit>\n"
							+ "            <ax21:bank_name>ACCESS BANK NIGERIA PLC</ax21:bank_name>\n"
							+ "            <ax21:bic/>\n" + "            <ax21:country_code>NG</ax21:country_code>\n"
							+ "            <ax21:currency_code>NGN</ax21:currency_code>\n"
							+ "            <ax21:dom_bank_code>044</ax21:dom_bank_code>\n"
							+ "            <ax21:iban/>\n"
							+ "            <ax21:mfs_bank_code>044</ax21:mfs_bank_code>\n" + "         </ns:return>\n"
							+ "         <ns:return xsi:type=\"ax21:Bank\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
							+ "            <ax21:bank_limit xsi:type=\"ax21:Limit\">\n"
							+ "               <ax21:max_daily_value>1000000.0</ax21:max_daily_value>\n"
							+ "               <ax21:max_monthly_value>7000000.0</ax21:max_monthly_value>\n"
							+ "               <ax21:max_per_tx_limit>250000.0</ax21:max_per_tx_limit>\n"
							+ "               <ax21:max_weekly_value>5000000.0</ax21:max_weekly_value>\n"
							+ "               <ax21:min_per_tx_limit>1000.0</ax21:min_per_tx_limit>\n"
							+ "            </ax21:bank_limit>\n"
							+ "            <ax21:bank_name>DIAMOND BANK LTD</ax21:bank_name>\n"
							+ "            <ax21:bic/>\n" + "            <ax21:country_code>NG</ax21:country_code>\n"
							+ "            <ax21:currency_code>NGN</ax21:currency_code>\n"
							+ "            <ax21:dom_bank_code>063</ax21:dom_bank_code>\n"
							+ "            <ax21:iban/>\n"
							+ "            <ax21:mfs_bank_code>063</ax21:mfs_bank_code>\n" + "         </ns:return>\n"
							+ "	</ns:get_banksResponse>\n" + "   </soapenv:Body>\n" + "</soapenv:Envelope>");

			if (httpsConResult != null) {

				serviceResponse = httpsConResult.getResponseData();

			}
			LOGGER.info(
					"==>In MFSApiCLient GetBanksServiceImpl of getBanks function mfs get_banks response for to_country "
							+ request.getToCountry() + "   " + serviceResponse);

			if (httpsConResult == null) {

				response.setErrorCode(ResponseCodes.ER215.getCode());
				response.setErrorMessage(ResponseCodes.ER215.getMessage());

			} else {

				if (httpsConResult.getResponseData() != null) {

					JSONObject obj = XML.toJSONObject(serviceResponse);
					String soapResponse = obj.getJSONObject("soapenv:Envelope").getJSONObject("soapenv:Body")
							.getJSONObject("ns:get_banksResponse").toString();

					JSONObject obj1 = new JSONObject(soapResponse);
					String returnResponse = obj1.getJSONArray("ns:return").toString();

					JSONArray array = new JSONArray(returnResponse);
					int i = 0;
					JSONObject jsonObject = new JSONObject();
					while (i < array.length()) {

						jsonObject = array.getJSONObject(i);
//						String bankLimit1 = jsonObject.get("ax21:bank_limit").toString();
//						JSONObject jsonObject1 = new JSONObject(bankLimit1);
//
//						String maxDailyValue = String.valueOf(jsonObject1.get("ax21:max_daily_value"));
//						String maxMonthlyValue = String.valueOf(jsonObject1.get("ax21:max_monthly_value"));
//						String maxPerTxLimit = String.valueOf(jsonObject1.get("ax21:max_per_tx_limit"));
//						String maxWeeklyValue = String.valueOf(jsonObject1.get("ax21:max_weekly_value"));
//						String minPerTxLimit = String.valueOf(jsonObject1.get("ax21:min_per_tx_limit"));
//
//						bankLimit.setMax_daily_value(maxDailyValue);
//						bankLimit.setMax_monthly_value(maxMonthlyValue);
//						bankLimit.setMax_per_tx_limit(maxPerTxLimit);
//						bankLimit.setMax_weekly_value(maxWeeklyValue);
//						bankLimit.setMin_per_tx_limit(minPerTxLimit);
//						getBanksResponse.setBank_limit(bankLimit);

						String bankName = (String) jsonObject.get("ax21:bank_name");
						String bic = (String) jsonObject.get("ax21:bic");
						String countryCode = (String) jsonObject.get("ax21:country_code");
						String currencyCode = (String) jsonObject.get("ax21:currency_code");
						String domBankCode = (String) jsonObject.get("ax21:dom_bank_code");
						String iBan = (String) jsonObject.get("ax21:iban");
						String mfsBankCode = (String) jsonObject.get("ax21:mfs_bank_code");

						getBanksResponse = new GetBanksResponseDto();
						getBanksResponse.setBank_name(bankName);
						getBanksResponse.setBic(bic);
						getBanksResponse.setCountry_code(countryCode);
						getBanksResponse.setCurrency_code(currencyCode);
						getBanksResponse.setDom_bank_code(domBankCode);
						getBanksResponse.setIban(iBan);
						getBanksResponse.setMfs_bank_code(mfsBankCode);

						list.add(getBanksResponse);
						i++;
					}
					response.setGetBanks(list);

				} else {

					response.setErrorCode(String.valueOf(httpsConResult.getRespCode()));
					response.setErrorMessage(httpsConResult.getTxMessage());

				}
			}

		} catch (Exception e) {
			LOGGER.error("==>Exception in GetBanksServiceImpl", e);
			response.setErrorCode(ResponseCodes.ER201.getCode());
			response.setErrorMessage(ResponseCodes.ER201.getMessage());

		}

		return response;
	}

	static String getXmlFromSOAPMessage(SOAPMessage paramSOAPMessage) throws SOAPException, IOException {
		ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
		paramSOAPMessage.writeTo(localByteArrayOutputStream);
		return new String(localByteArrayOutputStream.toByteArray());
	}

}
