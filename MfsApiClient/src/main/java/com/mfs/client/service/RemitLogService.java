package com.mfs.client.service;

import com.mfs.client.dto.MmRemitLogRequestDto;
import com.mfs.client.dto.MmRemitLogResponseDto;

public interface RemitLogService {

	public MmRemitLogResponseDto mmRemitLog(MmRemitLogRequestDto request);

	
	
}
