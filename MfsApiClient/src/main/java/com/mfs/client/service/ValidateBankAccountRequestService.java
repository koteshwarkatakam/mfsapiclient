package com.mfs.client.service;

import javax.xml.soap.SOAPException;

import com.mfs.client.dto.ValidateBankAccountRequestDto;
import com.mfs.client.dto.ValidateBankAccountResponseDto;

public interface ValidateBankAccountRequestService {
	
	public ValidateBankAccountResponseDto validateBankAccountRequest(ValidateBankAccountRequestDto request) throws SOAPException, Exception;

}
