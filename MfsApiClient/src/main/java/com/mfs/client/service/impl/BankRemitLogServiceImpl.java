package com.mfs.client.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.stereotype.Service;

import com.mfs.client.dto.BankRemitLogRequestDto;
import com.mfs.client.dto.BankRemitLogResponseDto;
import com.mfs.client.dto.ResponseStatus;
import com.mfs.client.dto.ReturnBankRemitLog;
import com.mfs.client.dto.Status;
import com.mfs.client.exception.CommonException;
import com.mfs.client.service.BankRemitLogService;
import com.mfs.client.util.CallService;
import com.mfs.client.util.CommonConstant;
import com.mfs.client.util.HttpsConnectionRequest;
import com.mfs.client.util.HttpsConnectionResponse;
import com.mfs.client.util.ResponseCodes;

@Service("BankRemitLogService")
public class BankRemitLogServiceImpl implements BankRemitLogService {
	private static final Logger LOGGER = LogManager.getLogger(BankRemitLogServiceImpl.class);

	public BankRemitLogResponseDto bankRemitLog(BankRemitLogRequestDto request) {

		String soapRequest = "";
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		Map<String, String> header = new HashMap<String, String>();
		HttpsConnectionResponse httpsConResult = new HttpsConnectionResponse();
		String serviceResponse = "";
		BankRemitLogResponseDto response = new BankRemitLogResponseDto();

		try {
			// set soap request
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage soapMessage = messageFactory.createMessage();
			SOAPPart soapPart = soapMessage.getSOAPPart();

			SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
			soapEnvelope.addNamespaceDeclaration("ws", "http://ws.mfsafrica.com");

			SOAPEnvelope soapEnvelope1 = soapPart.getEnvelope();
			soapEnvelope1.addNamespaceDeclaration("xsd", "http://mfs/xsd");

			SOAPHeader localSOAPHeader = soapEnvelope.getHeader();

			SOAPBody localSOAPBody = soapEnvelope.getBody();

			SOAPElement localSOAPElement1 = localSOAPBody.addChildElement("bank_remit_log", "ws");

			SOAPElement localSOAPElement3 = localSOAPElement1.addChildElement("login", "ws");

			SOAPElement localSOAPElement6 = localSOAPElement3.addChildElement("corporate_code", "xsd");
			localSOAPElement6.addTextNode(request.getLogin().getCorporate_code());

			SOAPElement localSOAPElement7 = localSOAPElement3.addChildElement("password", "xsd");
			localSOAPElement7.addTextNode("");

			SOAPElement localSOAPElement4 = localSOAPElement1.addChildElement("receive_amount", "ws");

			SOAPElement localSOAPElement8 = localSOAPElement4.addChildElement("amount", "xsd");
			localSOAPElement8.addTextNode(String.valueOf(request.getReceive_amount().getAmount()));

			SOAPElement localSOAPElement9 = localSOAPElement4.addChildElement("currency_code", "xsd");
			localSOAPElement9.addTextNode(request.getReceive_amount().getCurrency_code());

			SOAPElement localSOAPElement5 = localSOAPElement1.addChildElement("sender", "ws");

			SOAPElement localSOAPElement10 = localSOAPElement5.addChildElement("address", "xsd");
			localSOAPElement10.addTextNode(request.getSender().getAddress());

			SOAPElement localSOAPElement11 = localSOAPElement5.addChildElement("city", "xsd");
			localSOAPElement11.addTextNode(request.getSender().getCity());

			SOAPElement localSOAPElement12 = localSOAPElement5.addChildElement("date_of_birth", "xsd");
			localSOAPElement12.addTextNode(request.getSender().getDate_of_birth());

			SOAPElement localSOAPElement13 = localSOAPElement5.addChildElement("document", "xsd");

			SOAPElement localSOAPElement14 = localSOAPElement13.addChildElement("id_country", "xsd");
			localSOAPElement14.addTextNode(request.getSender().getDocument().getId_country());

			SOAPElement localSOAPElement15 = localSOAPElement13.addChildElement("id_expiry", "xsd");
			localSOAPElement15.addTextNode(request.getSender().getDocument().getId_expiry());

			SOAPElement localSOAPElement16 = localSOAPElement13.addChildElement("id_number", "xsd");
			localSOAPElement16.addTextNode(request.getSender().getDocument().getId_number());

			SOAPElement localSOAPElement17 = localSOAPElement13.addChildElement("id_type", "xsd");
			localSOAPElement17.addTextNode(request.getSender().getDocument().getId_type());

			SOAPElement localSOAPElement18 = localSOAPElement5.addChildElement("email", "xsd");
			localSOAPElement18.addTextNode(request.getSender().getEmail());

			SOAPElement localSOAPElement19 = localSOAPElement5.addChildElement("from_country", "xsd");
			localSOAPElement19.addTextNode(request.getSender().getFrom_country());

			SOAPElement localSOAPElement20 = localSOAPElement5.addChildElement("msisdn", "xsd");
			localSOAPElement20.addTextNode(request.getSender().getMsisdn());

			SOAPElement localSOAPElement21 = localSOAPElement5.addChildElement("name", "xsd");
			localSOAPElement21.addTextNode(request.getSender().getName());

			SOAPElement localSOAPElement22 = localSOAPElement5.addChildElement("postal_code", "xsd");
			localSOAPElement22.addTextNode(request.getSender().getPostal_code());

			SOAPElement localSOAPElement23 = localSOAPElement5.addChildElement("state", "xsd");
			localSOAPElement23.addTextNode(request.getSender().getState());

			SOAPElement localSOAPElement24 = localSOAPElement5.addChildElement("surname", "xsd");
			localSOAPElement24.addTextNode(request.getSender().getSurname());

			SOAPElement localSOAPElement25 = localSOAPElement1.addChildElement("recipient", "ws");

			SOAPElement localSOAPElement26 = localSOAPElement25.addChildElement("address", "xsd");
			localSOAPElement26.addTextNode(request.getRecipient().getAddress());

			SOAPElement localSOAPElement27 = localSOAPElement25.addChildElement("city", "xsd");
			localSOAPElement27.addTextNode(request.getRecipient().getCity());

			SOAPElement localSOAPElement28 = localSOAPElement25.addChildElement("date_of_birth", "xsd");
			localSOAPElement28.addTextNode(request.getRecipient().getDate_of_birth());

			SOAPElement localSOAPElement29 = localSOAPElement25.addChildElement("document", "xsd");

			SOAPElement localSOAPElement30 = localSOAPElement29.addChildElement("id_country", "xsd");
			localSOAPElement30.addTextNode(request.getRecipient().getDocument().getId_country());

			SOAPElement localSOAPElement31 = localSOAPElement29.addChildElement("id_expiry", "xsd");
			localSOAPElement31.addTextNode(request.getRecipient().getDocument().getId_expiry());

			SOAPElement localSOAPElement32 = localSOAPElement29.addChildElement("id_number", "xsd");
			localSOAPElement32.addTextNode(request.getRecipient().getDocument().getId_number());

			SOAPElement localSOAPElement33 = localSOAPElement29.addChildElement("id_type", "xsd");
			localSOAPElement33.addTextNode(request.getRecipient().getDocument().getId_type());

			SOAPElement localSOAPElement34 = localSOAPElement25.addChildElement("email", "xsd");
			localSOAPElement34.addTextNode(request.getRecipient().getEmail());

			SOAPElement localSOAPElement36 = localSOAPElement25.addChildElement("msisdn", "xsd");
			localSOAPElement36.addTextNode(request.getRecipient().getMsisdn());

			SOAPElement localSOAPElement37 = localSOAPElement25.addChildElement("name", "xsd");
			localSOAPElement37.addTextNode(request.getRecipient().getName());

			SOAPElement localSOAPElement38 = localSOAPElement25.addChildElement("postal_code", "xsd");
			localSOAPElement38.addTextNode(request.getRecipient().getPostal_code());

			SOAPElement localSOAPElement39 = localSOAPElement25.addChildElement("state", "xsd");
			localSOAPElement39.addTextNode(request.getRecipient().getState());

			SOAPElement localSOAPElement40 = localSOAPElement25.addChildElement("status", "xsd");

			SOAPElement localSOAPElement41 = localSOAPElement40.addChildElement("status_code", "xsd");
			localSOAPElement41.addTextNode(request.getRecipient().getStatus().getCode());

			SOAPElement localSOAPElement42 = localSOAPElement25.addChildElement("surname", "xsd");
			localSOAPElement42.addTextNode(request.getRecipient().getSurname());

			SOAPElement localSOAPElement43 = localSOAPElement25.addChildElement("to_country", "xsd");
			localSOAPElement43.addTextNode(request.getRecipient().getTo_country());

			SOAPElement localSOAPElement45 = localSOAPElement1.addChildElement("account", "ws");

			SOAPElement localSOAPElement46 = localSOAPElement45.addChildElement("account_number", "xsd");
			localSOAPElement46.addTextNode(request.getAccount().getAccount_number());

			SOAPElement localSOAPElement47 = localSOAPElement45.addChildElement("mfs_bank_code", "xsd");
			localSOAPElement47.addTextNode(request.getAccount().getMfs_bank_code());

			SOAPElement localSOAPElement44 = localSOAPElement1.addChildElement("third_party_trans_id", "ws");
			localSOAPElement44.addTextNode(request.getThird_party_trans_id());

			SOAPElement localSOAPElement2 = localSOAPElement1.addChildElement("reference", "ws");
			localSOAPElement2.addTextNode(request.getReference());

			soapRequest = getXmlFromSOAPMessage(soapMessage);

			LOGGER.info("==>In RemitLogServiceImpl  of bankRemitLog function request"
					+ request.getThird_party_trans_id() + "           " + soapRequest);

			soapRequest = soapRequest.replace("<xsd:password/>",
					"<xsd:password>" + request.getLogin().getPassword() + "</xsd:password>");

			connectionRequest.setServiceUrl(request.getWsdl());
			header.put(CommonConstant.CONTENT_TYPE, CommonConstant.TEXT_XML);
			if (request.getxApiKey() != null) {

				header.put(CommonConstant.X_API_KEY, request.getxApiKey());
			}
			connectionRequest.setHeaders(header);

			// call to mfs api
			try {
				//httpsConResult = CallService.getResponseFromService(connectionRequest, soapRequest);
				serviceResponse="<soapenv:Envelope\r\n\txmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n\t<soapenv:Body>\r\n\t\t<ns:bank_remit_logResponse\r\n\t\t\txmlns:ns=\"http://ws.mfsafrica.com\">\r\n\t\t\t<ns:return xsi:type=\"ax21:TransactionBank\"\r\n\t\t\t\txmlns:ax21=\"http://mfs/xsd\"\r\n\t\t\t\txmlns:ax23=\"http://airtime/xsd\"\r\n\t\t\t\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\r\n\t\t\t\t<ax21:fx_rate>20</ax21:fx_rate>\r\n\t\t\t\t<ax21:mfs_trans_id>MFS001test</ax21:mfs_trans_id>\r\n\t\t\t\t<ax21:receive_amount xsi:type=\"ax21:Money\">\r\n\t\t\t\t\t<ax21:amount>10</ax21:amount>\r\n\t\t\t\t\t<ax21:currency_code>USD</ax21:currency_code>\r\n\t\t\t\t</ax21:receive_amount>\r\n\t\t\t\t<ax21:send_amount xsi:nil=\"true\"/>\r\n\t\t\t\t<ax21:third_party_trans_id>XC09782346</ax21:third_party_trans_id>\r\n\t\t\t\t<ax21:status>\r\n\t\t\t\t\t<ax21:code>\r\n\t\t\t\t\tMR101\r\n\t\t\t\t\t</code>\r\n\t\t\t\t\t<ax21:message>\r\n\t\t\t\t\tSUCCESS\r\n\t\t\t\t\t</message>\r\n\t\t\t\t</ax21:status>\r\n\t\t\t</ns:return>\r\n\t\t</ns:bank_remit_logResponse>\r\n\t</soapenv:Body>\r\n</soapenv:Envelope>";
				httpsConResult = new HttpsConnectionResponse();
				httpsConResult.setRespCode(200);
				httpsConResult.setResponseData(serviceResponse);
			} /*catch (IOException e) {*/
				catch (Exception e) {
				LOGGER.error("IO Exception occured while connecting :: ", e);
				Throwable throwable = e.getCause();
				if (throwable instanceof SocketTimeoutException) {
					LOGGER.info("SocketTimeoutException, with 60 sec waitime... !!");
					try {
						LOGGER.info("Remit log started after wait of 60 sec... and Third_party_trans_id is :: "
								+ request.getThird_party_trans_id());
						httpsConResult = CallService.getResponseFromService(connectionRequest, soapRequest);
						LOGGER.info("Remit log success in retry for Third_party_trans_id :: "
								+ request.getThird_party_trans_id());
					} catch (Exception e1) {
						LOGGER.error("Exception occured in retry while logging transaction in MFS API :: ", e1);
						// In case of log FAIL, set world remit error with 0033 and set fail.
						ResponseStatus status = new ResponseStatus();
						status.setStatusCode(ResponseCodes.ERR203.getCode());
						status.setStatusMessage(ResponseCodes.ERR203.getMessage());
						throw new CommonException(status);
					}
				} else {
					LOGGER.error("Other IO Exception occured while connecting to MFS API :: ", e);
					// In case of log FAIL, set world remit error with 0033 and set fail.
					ResponseStatus status = new ResponseStatus();
					status.setStatusCode(ResponseCodes.ERR203.getCode());
					status.setStatusMessage(ResponseCodes.ERR203.getMessage());
					throw new CommonException(status);
				}
			}

			if (httpsConResult != null) {
				serviceResponse = httpsConResult.getResponseData();
			}
			LOGGER.info("==>In RemitLogServiceImpl of bankRemitLog function mfs mm_remit_log response "
					+ request.getThird_party_trans_id() + "      " + serviceResponse);

			if (httpsConResult == null) {

				response.setCode(ResponseCodes.ER215.getCode());
				response.setMessage(ResponseCodes.ER215.getMessage());

			} else {

				if (httpsConResult.getResponseData() != null) {

					serviceResponse = httpsConResult.getResponseData();
					JSONObject obj = XML.toJSONObject(serviceResponse);
					String jso1 = obj.getJSONObject("soapenv:Envelope").getJSONObject("soapenv:Body")
							.getJSONObject("ns:bank_remit_logResponse").getJSONObject("ns:return").toString();

					ReturnBankRemitLog returnDto = new ReturnBankRemitLog();
					Status status = new Status();
					JSONObject obj2 = new JSONObject(jso1);

					String mfsTransId = String.valueOf(obj2.get("ax21:mfs_trans_id"));

					if (jso1.contains("status_code")) {

						String statusCode = (String) obj2.getJSONObject("ax21:status").get("ax21:status_code");
						String message = (String) obj2.getJSONObject("ax21:status").get("ax21:message");

						status.setCode(statusCode);
						status.setMessage(message);
						returnDto.setStatus(status);
					}

					returnDto.setMfs_trans_id(mfsTransId);
					response.setReturnResponse(returnDto);

				} else {

					response.setCode(String.valueOf(httpsConResult.getRespCode()));

					if (httpsConResult.getRespCode() == 0) {
						response.setMessage(ResponseCodes.COMMIT_QUEUED.getMessage());
					} else {

						response.setMessage(httpsConResult.getTxMessage());

					}

				}
			}
		} catch (CommonException ce) {
			LOGGER.error("==>CommonException in MFSApiClient of RemitLogServiceImpl", ce);
			response.setCode(ce.getStatus().getStatusCode());
			response.setMessage(ce.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in MFSApiClient of RemitLogServiceImpl", e);
			response.setCode(ResponseCodes.ER201.getCode());
			response.setMessage(ResponseCodes.ER201.getMessage());
		}

		return response;
	}

	static String getXmlFromSOAPMessage(SOAPMessage paramSOAPMessage) throws SOAPException, IOException {
		ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
		paramSOAPMessage.writeTo(localByteArrayOutputStream);
		return new String(localByteArrayOutputStream.toByteArray());
	}

}
