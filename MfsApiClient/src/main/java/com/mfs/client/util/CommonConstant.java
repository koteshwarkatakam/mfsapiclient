package com.mfs.client.util;
public class CommonConstant {

	public static final String CONTENT_TYPE = "Content-type";
	public static final String TEXT_XML = "text/xml";
	public static final String X_API_KEY = "x-api-key";
	public static final String MFS_LOG_FAIL_TRANS_ID = "00000000";
	public static final String MFS_LOG_FAIL_VALUE = "\"xsi:nil\":true";
	public static final String LOG_FAIL = "Log Fail";

}
