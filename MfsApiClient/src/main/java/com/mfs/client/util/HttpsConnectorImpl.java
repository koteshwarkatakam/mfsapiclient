package com.mfs.client.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



public class HttpsConnectorImpl {

	private static final Logger LOGGER = LogManager.getLogger(HttpsConnectorImpl.class);

	/**
	 * @param connectURL
	 * @param data
	 * @return
	 * @throws Exception
	 * @throws ProtocolException
	 * @throws MTAdapterConEx
	 */

	public HttpsConnectionResponse connectionUsingHTTPS(String connectionData,
			final HttpsConnectionRequest httpsConnectionRequest) throws Exception {
		SSLContext sslContext = null;
		int responseCode = 0;

		HttpsConnectionResponse connectionResponse = new HttpsConnectionResponse();
		try {
			sslContext = SSLContext.getInstance("TLSv1.2");
			sslContext.init(null, new javax.net.ssl.TrustManager[] { new TrustAllTrustManager() },
					new java.security.SecureRandom());
		} catch (Exception e) {
			LOGGER.error("Exception occured while setting SSL factory", e);
		}

		URL httpsUrl = new URL(httpsConnectionRequest.getServiceUrl());

		HttpsURLConnection httpsConnection = (HttpsURLConnection) httpsUrl.openConnection();

		httpsConnection.setSSLSocketFactory(sslContext.getSocketFactory());

		httpsConnection.setRequestMethod(httpsConnectionRequest.getHttpmethodName());

		if (connectionData.contains("account_request") || connectionData.contains("validate_bank_account")) {
			// Read timeout Changed to 30 sec
			httpsConnection.setReadTimeout(30000);
		} else {
			// Read timeout Changed to 60 sec
			httpsConnection.setReadTimeout(60000);
		}

		httpsConnection.setDoOutput(true);
		httpsConnection.setDoInput(true);

		setHeaderPropertiesToHTTPSConnection(httpsConnectionRequest, connectionData, httpsConnection);
		try {
			if (connectionData != null) {
				DataOutputStream wr = new DataOutputStream(httpsConnection.getOutputStream());
				wr.writeBytes(connectionData);
				wr.flush();
				wr.close();
			}
			responseCode = httpsConnection.getResponseCode();
		} catch (Exception e) {
			LOGGER.error("In HttpsConnectorImpl Response Status while connecting MFS readtimeout-->", e);
			
			int respCode;
			if (connectionData.contains("mm_remit_log") || connectionData.contains("trans_com")) {
				respCode = retry(connectionData, httpsConnectionRequest);

				if (respCode == 0) {

					connectionResponse.setRespCode(respCode);

					return connectionResponse;

				}
			} else if (connectionData.contains("bank_remit_log")) {

				connectionResponse = retryBankTransfer(connectionData, httpsConnectionRequest);

				if (connectionResponse.getRespCode() != 0) {

					if (connectionResponse.getResponseData().contains("00000000")) {

						connectionResponse = retryBankTransfer(connectionData, httpsConnectionRequest);
					}

				}

				return connectionResponse;

			}

			// throw new Exception(e);

		}
		LOGGER.info("In HttpsConnectorImpl Response Status while connecting MFS -->" + responseCode);

		connectionResponse.setRespCode(responseCode);
		BufferedReader httpsResponse = null;

		if (responseCode == HttpsResponseCodes.S200.getCode()) {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));

		} else if (responseCode == HttpsResponseCodes.ER204.getCode()) {

			connectionResponse.setTxMessage(HttpsResponseCodes.ER204.getMessage());
			connectionResponse.setRespCode(responseCode);
			LOGGER.error("Error in httpsConnectorImpl " + connectionResponse.getTxMessage());

		} else if (responseCode == HttpsResponseCodes.ER400.getCode()) {

			connectionResponse.setTxMessage(HttpsResponseCodes.ER400.getMessage());
			connectionResponse.setRespCode(responseCode);
			LOGGER.error("Error in httpsConnectorImpl " + connectionResponse.getTxMessage());

		} else if (responseCode == HttpsResponseCodes.ER403.getCode()) {

			connectionResponse.setTxMessage(HttpsResponseCodes.ER403.getMessage());
			connectionResponse.setRespCode(responseCode);
			LOGGER.error("Error in httpsConnectorImpl " + connectionResponse.getTxMessage());

		} else if (responseCode == HttpsResponseCodes.ER401.getCode()) {
			connectionResponse.setTxMessage(HttpsResponseCodes.ER401.getMessage());
			connectionResponse.setRespCode(responseCode);
			LOGGER.error("Error in httpsConnectorImpl " + connectionResponse.getTxMessage());

		} else if (responseCode == HttpsResponseCodes.ER500.getCode()) {

			connectionResponse.setTxMessage(HttpsResponseCodes.ER500.getMessage());
			connectionResponse.setRespCode(responseCode);
			LOGGER.error("Error in httpsConnectorImpl " + connectionResponse.getTxMessage());

		} else {

			String errMessage = null;
			if (responseCode != 0) {
				errMessage = getInputAsString(httpsConnection.getErrorStream());
				connectionResponse.setRespCode(responseCode);
				connectionResponse.setTxMessage(errMessage);
				LOGGER.error("ErrorMessage in connection " + errMessage);

			} else {
				connectionResponse.setRespCode(responseCode);
				connectionResponse.setTxMessage("Other fail");
			}

		}

		String output = null;
		if (httpsResponse != null) {
			output = readConnectionDataWithBuffer(httpsResponse);

			if (output.contains("bank_remit_logResponse") && output.contains("00000000")) {

				connectionResponse = connectionUsingHTTPSRetry(connectionData, httpsConnectionRequest);

				return connectionResponse;

			}
		}

		if (httpsResponse != null) {
			httpsResponse.close();
		}
		if (output != null) {
			connectionResponse.setResponseData(output);
		}

		LOGGER.debug("Response -->" + output);

		return connectionResponse;
	}

	private HttpsConnectionResponse connectionUsingHTTPSRetry(String connectionData,
			final HttpsConnectionRequest httpsConnectionRequest) throws UnknownHostException, IOException {
		SSLContext sslContext = null;
		HttpsConnectionResponse connectionResponse = new HttpsConnectionResponse();

		try {
			sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, new javax.net.ssl.TrustManager[] { new TrustAllTrustManager() },
					new java.security.SecureRandom());
		} catch (Exception e) {
			LOGGER.error("Exception occured while setting SSL factory", e);
		}

		URL httpsUrl = new URL(httpsConnectionRequest.getServiceUrl());

		// sslContext.getSocketFactory().createSocket(httpsUrl.getHost(),
		// httpsConnectionRequest.getPort());

		HttpsURLConnection httpsConnection = (HttpsURLConnection) httpsUrl.openConnection();

		httpsConnection.setSSLSocketFactory(sslContext.getSocketFactory());

		httpsConnection.setRequestMethod(httpsConnectionRequest.getHttpmethodName());

		// Read timeout Changed to 60 sec
		httpsConnection.setReadTimeout(60000);

		httpsConnection.setDoOutput(true);
		httpsConnection.setDoInput(true);

		setHeaderPropertiesToHTTPSConnection(httpsConnectionRequest, connectionData, httpsConnection);

		if (connectionData != null) {

			DataOutputStream wr = new DataOutputStream(httpsConnection.getOutputStream());

			wr.writeBytes(connectionData);
			wr.flush();
			wr.close();
		}
		int responseCode = httpsConnection.getResponseCode();

		LOGGER.info("In HttpsConnectorImpl on connectionUsingHTTPSRetry Response Status while connecting MFS"
				+ responseCode);

		connectionResponse.setRespCode(responseCode);

		BufferedReader httpsResponse = null;

		if (responseCode == HttpsResponseCodes.S200.getCode()) {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));

		} else {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.error("ErrorMessage " + errMessage);

			// connectionResponse.setRespCode(responseCode);
			// connectionResponse.setTxMessage(errMessage);

		}

		String output = null;
		if (httpsResponse != null) {
			output = readConnectionDataWithBuffer(httpsResponse);
		}

		if (httpsResponse != null) {
			httpsResponse.close();
		}
		if (output != null) {
			connectionResponse.setResponseData(output);
		}

		LOGGER.error("End of connectionUsingHTTPSRetry() in HttpsConnectorImpl");
		return connectionResponse;
	}

	public static void wait(int seconds) {
		try {
			LOGGER.error("Waiting " + seconds + " seconds ..");
			Thread.sleep(seconds);
		} catch (InterruptedException e) {
			LOGGER.error("Exception Occured in HttpsConnectorImpl :: ", e);
		}
	}

	private String readConnectionDataWithBuffer(BufferedReader httpsResponse) throws IOException {
		String output;
		String responseLine;
		StringBuffer response = new StringBuffer();

		while ((responseLine = httpsResponse.readLine()) != null) {
			response.append(responseLine);
		}

		output = response.toString();
		return output;
	}

	private void setHeaderPropertiesToHTTPSConnection(final HttpsConnectionRequest httpsConnectionRequest,
			String connectionData, HttpsURLConnection httpsConnection) {

		setHeadersToHTTPSConnection(httpsConnectionRequest.getHeaders(), httpsConnection);

		if (connectionData != null && connectionData.length() > 0) {

			httpsConnection.setRequestProperty("Content-Length", String.valueOf(connectionData.length()));
		} else {
			httpsConnection.setRequestProperty("Content-Length", "0");
		}

	}

	private void setHeadersToHTTPSConnection(Map<String, String> headers, HttpsURLConnection httpsConnection) {

		if (headers != null && !headers.isEmpty() && httpsConnection != null) {

			for (Entry<String, String> entry : headers.entrySet()) {

				httpsConnection.setRequestProperty(entry.getKey(), entry.getValue());
			}
		}
	}

	private static String getInputAsString(InputStream paramInputStream) {
		Scanner localScanner = new Scanner(paramInputStream);
		return localScanner.useDelimiter("\\A").hasNext() ? localScanner.next() : "";
	}

	private int retry(String connectionData, final HttpsConnectionRequest httpsConnectionRequest)
			throws InterruptedException, UnknownHostException, IOException {
		HttpsConnectionResponse connectionResponse = new HttpsConnectionResponse();

		int retryCount = 1;
		final int maxRetry = 3;
		for (int i = retryCount; i <= maxRetry; i++) {
			try {
				Thread.sleep(60000);
				connectionResponse = connectionUsingHTTPSRetry(connectionData, httpsConnectionRequest);
			} catch (Exception e) {
				LOGGER.error("In HttpsConnectorImpl Response Status while connecting MFS readtimeout-->" + e);
				retryCount++;

				continue;
			}
			break;
		}

		return connectionResponse.getRespCode();

	}

	private HttpsConnectionResponse retryBankTransfer(String connectionData,
			final HttpsConnectionRequest httpsConnectionRequest)
			throws InterruptedException, UnknownHostException, IOException {
		HttpsConnectionResponse connectionResponse = new HttpsConnectionResponse();

		int retryCount = 0;
		final int maxRetry = 1;
		for (int i = retryCount; i < maxRetry; i++) {
			try {

				Thread.sleep(60000);
				connectionResponse = connectionUsingHTTPSRetry(connectionData, httpsConnectionRequest);
			} catch (Exception e) {
				LOGGER.error("In HttpsConnectorImpl Response Status while connecting MFS readtimeout-->" + e);
				retryCount++;

				continue;
			}
			break;
		}

		return connectionResponse;

	}

}