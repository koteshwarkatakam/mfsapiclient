package com.mfs.client.util;

public enum HttpsResponseCodes {
	
	
	S200(200,"OK"),
	ER204(204,"No Content"),
	ER400(400,"Bad Request"),
	ER401(401,"Unauthorized"),
	ER403(403,"Forbidden"),
	ER408(408,"Request Timeout"),
	ER500(500,"Internal Server Error");
	
	private int code;
	private String message;
	
	public int getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}

	private HttpsResponseCodes(int code, String message) {
		this.code = code;
		this.message = message;
	}


}
