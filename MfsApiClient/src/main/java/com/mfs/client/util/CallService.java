package com.mfs.client.util;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mfs.client.dto.ResponseStatus;
import com.mfs.client.exception.CommonException;



public class CallService {

	private static final Logger LOGGER = LogManager.getLogger(CallService.class);

	public static HttpsConnectionResponse getResponseFromService(HttpsConnectionRequest connectionRequest,
			String request) throws Exception {

		HttpsConnectionResponse httpsConResult = null;

		try {

			connectionRequest.setHttpmethodName("POST");
			// boolean isHttps = true;
			if (connectionRequest.getServiceUrl().startsWith("https")) {
				HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
				httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
			} else {
				HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
				httpsConResult = httpConnectorImpl.httpUrlConnection(connectionRequest, request);
			}
		} catch (Exception e) {
			LOGGER.error("==>Exception thrown in CallServices getResponseFromService Method  : ", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(ResponseCodes.ERR203.getCode());
			status.setStatusMessage(ResponseCodes.ERR203.getMessage());
			throw new CommonException(status);
		}
		return httpsConResult;
	}

	//
	// public static String getResponseFromService(String url,String signature)
	// throws Exception {
	//
	// HttpsConnectionResponse httpsConResult = null;
	// String str_result = null;
	//
	// LOGGER.info("In Callservices in getResponseFromService(url,signature) Request
	// Body =>");
	// LOGGER.info("url= "+url +" "+"signature= "+ signature);
	// try {
	// HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
	//
	// Map<String, String> headerMap = new HashMap<String, String>();
	//
	// headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.TEXT_XML);
	// //headerMap.put(CommonConstant.AUTHORIZATION, signature);
	//
	// LOGGER.debug("URL : "+url);
	// connectionRequest.setServiceUrl(url);
	// connectionRequest.setHeaders(headerMap);
	// connectionRequest.setHttpmethodName("POST");
	//
	// HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
	// httpsConResult=httpConnectorImpl.httpUrlConnection(connectionRequest,signature);
	// str_result=httpsConResult.getResponseData();
	//
	// LOGGER.info("In Callservices in getResponseFromService(url,accessToken)
	// Response Body =>");
	// LOGGER.info(str_result);
	// } catch (Exception e) {
	// LOGGER.error("==>Exception thrown in CallServices in
	// getResponseFromService(url,accessToken) : "+e);
	// throw new Exception(e);
	// }
	// return str_result;
	// }
}
