package com.mfs.client.util;
public enum ResponseCodes {
	
	ER201("ER201", "System Error"), 
	ER202("ER202", "Remmittance Internal Error"),
	ER204("ER204", "Error while saving data"), 
	ER205("ER205", "Error while updating data"),
	VALIDATION_ERROR("ER206", "Validation Error"),
	S200("200" , "Success"),
	ER207("ER207", "System Error"),
	ER208("ER208", "Error while getting last record"),
	ER209("ER209","No record found against externalReferenceId"),
	ER210("ER210","No record found against externalRefId and serviceName"),
	ER211("ER211","Duplicate record found i.e externalReferenceId"),
	ER212("ER212", "Error while getting ExtTransactionId"),
	ER213("ER213", "No record found againest extTransactionID"),
	ER214("ER214","Error while getting externalTransactionId and serviceName"),
	ERR203("ER203","Error while connecting MFS"),
	COMMIT_QUEUED("Transaction Queued" , "Transaction Queued"),
	ER215("ER215", "Null response");

	private String code;
	private String message;
	
	private ResponseCodes(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
