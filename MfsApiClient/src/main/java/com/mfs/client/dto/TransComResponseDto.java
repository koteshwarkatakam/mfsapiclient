package com.mfs.client.dto;

public class TransComResponseDto 
{
	private ReturnTransCom returnResponse;

	public ReturnTransCom getReturnResponse() {
		return returnResponse;
	}

	public void setReturnResponse(ReturnTransCom returnResponse) {
		this.returnResponse = returnResponse;
	}

	@Override
	public String toString() {
		return "TransComResponseDto [returnResponse=" + returnResponse + "]";
	}
	
	
}
