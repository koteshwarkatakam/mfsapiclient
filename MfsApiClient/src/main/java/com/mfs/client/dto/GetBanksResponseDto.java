package com.mfs.client.dto;

public class GetBanksResponseDto {

	private BankLimit bank_limit;

	private String bank_name;
	private String bic;
	private String country_code;
	private String currency_code;
	private String dom_bank_code;
	private String iban;
	private String mfs_bank_code;

	public BankLimit getBank_limit() {
		return bank_limit;
	}

	public void setBank_limit(BankLimit bank_limit) {
		this.bank_limit = bank_limit;
	}

	public String getBank_name() {
		return bank_name;
	}

	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getCountry_code() {
		return country_code;
	}

	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}

	public String getCurrency_code() {
		return currency_code;
	}

	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}

	public String getDom_bank_code() {
		return dom_bank_code;
	}

	public void setDom_bank_code(String dom_bank_code) {
		this.dom_bank_code = dom_bank_code;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getMfs_bank_code() {
		return mfs_bank_code;
	}

	public void setMfs_bank_code(String mfs_bank_code) {
		this.mfs_bank_code = mfs_bank_code;
	}

	@Override
	public String toString() {
		return "GetBanksResponseDto [bank_limit=" + bank_limit + ", bank_name=" + bank_name + ", bic=" + bic
				+ ", country_code=" + country_code + ", currency_code=" + currency_code + ", dom_bank_code="
				+ dom_bank_code + ", iban=" + iban + ", mfs_bank_code=" + mfs_bank_code + "]";
	}

}
