package com.mfs.client.dto;


public class Recipient {
	 private String address;

	    private String city;

	    private String date_of_birth;
	    
	    private Document document;

	    private String email;

	    private String msisdn;
	    
	    private String name;
	    
	    private String postal_code;
	    
	    private String state;

	    private Status status;
	    
	    private String surname;
	    
	    private String to_country;

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getDate_of_birth() {
			return date_of_birth;
		}

		public void setDate_of_birth(String date_of_birth) {
			this.date_of_birth = date_of_birth;
		}

		public String getSurname() {
			return surname;
		}

		public void setSurname(String surname) {
			this.surname = surname;
		}

		public Document getDocument() {
			return document;
		}

		public void setDocument(Document document) {
			this.document = document;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getTo_country() {
			return to_country;
		}

		public void setTo_country(String to_country) {
			this.to_country = to_country;
		}

		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		public String getMsisdn() {
			return msisdn;
		}

		public void setMsisdn(String msisdn) {
			this.msisdn = msisdn;
		}

		public String getPostal_code() {
			return postal_code;
		}

		public void setPostal_code(String postal_code) {
			this.postal_code = postal_code;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public Status getStatus() {
			return status;
		}

		public void setStatus(Status status) {
			this.status = status;
		}

		@Override
		public String toString() {
			return "Recipient [address=" + address + ", city=" + city + ", date_of_birth=" + date_of_birth
					+ ", surname=" + surname + ", name=" + name + ", to_country=" + to_country + ", state=" + state
					+ ", msisdn=" + msisdn + ", postal_code=" + postal_code + ", email=" + email + "]";
		}
	    
	    
}
