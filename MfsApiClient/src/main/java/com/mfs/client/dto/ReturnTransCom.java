package com.mfs.client.dto;

public class ReturnTransCom {
	private String code;

	private String e_trans_id;

	private String message;

	private String mfs_trans_id;

	private String third_party_trans_id;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMfs_trans_id() {
		return mfs_trans_id;
	}

	public void setMfs_trans_id(String mfs_trans_id) {
		this.mfs_trans_id = mfs_trans_id;
	}

	public String getE_trans_id() {
		return e_trans_id;
	}

	public void setE_trans_id(String e_trans_id) {
		this.e_trans_id = e_trans_id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getThird_party_trans_id() {
		return third_party_trans_id;
	}

	public void setThird_party_trans_id(String third_party_trans_id) {
		this.third_party_trans_id = third_party_trans_id;
	}

	@Override
	public String toString() {
		return "ReturnTransCom [code=" + code + ", mfs_trans_id=" + mfs_trans_id + ", e_trans_id=" + e_trans_id
				+ ", message=" + message + ", third_party_trans_id=" + third_party_trans_id + "]";
	}

}
