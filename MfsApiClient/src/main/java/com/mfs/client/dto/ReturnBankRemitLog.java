package com.mfs.client.dto;

public class ReturnBankRemitLog {

	private String mfs_trans_id;

	private Status status;

	public String getMfs_trans_id() {
		return mfs_trans_id;
	}

	public void setMfs_trans_id(String mfs_trans_id) {
		this.mfs_trans_id = mfs_trans_id;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ReturnBankRemitLog [mfs_trans_id=" + mfs_trans_id + ", status=" + status + "]";
	}

}
