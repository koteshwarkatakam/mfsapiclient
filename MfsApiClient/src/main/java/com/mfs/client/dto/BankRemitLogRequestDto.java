package com.mfs.client.dto;

public class BankRemitLogRequestDto {
	
	private String reference;

    private Sender sender;

    private Recipient recipient;
    
    private Account account;

    private Receive_amount receive_amount;

    private Login login;

    private String third_party_trans_id;
    
    private String wsdl;
    
    private String xApiKey;

    
    
	public String getWsdl() {
		return wsdl;
	}

	public void setWsdl(String wsdl) {
		this.wsdl = wsdl;
	}

	public String getxApiKey() {
		return xApiKey;
	}

	public void setxApiKey(String xApiKey) {
		this.xApiKey = xApiKey;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Sender getSender() {
		return sender;
	}

	public void setSender(Sender sender) {
		this.sender = sender;
	}

	public Recipient getRecipient() {
		return recipient;
	}

	public void setRecipient(Recipient recipient) {
		this.recipient = recipient;
	}

	public Receive_amount getReceive_amount() {
		return receive_amount;
	}

	public void setReceive_amount(Receive_amount receive_amount) {
		this.receive_amount = receive_amount;
	}

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public String getThird_party_trans_id() {
		return third_party_trans_id;
	}

	public void setThird_party_trans_id(String third_party_trans_id) {
		this.third_party_trans_id = third_party_trans_id;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Override
	public String toString() {
		return "MmRemitLogRequestDto [reference=" + reference + ", sender=" + sender + ", recipient=" + recipient
				+ ", account=" + account + ", receive_amount=" + receive_amount + ", login=" + login
				+ ", third_party_trans_id=" + third_party_trans_id + ", wsdl=" + wsdl + ", xApiKey=" + xApiKey + "]";
	}

	
    
}
