package com.mfs.client.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TransStatusRequestDto {

	private Login login;
	private String trans_id;
	private String wsdl;
	private String xApiKey;

	public Login getLogin() {
		return login;
	}

	public void setLogin(Login login) {
		this.login = login;
	}

	public String getTrans_id() {
		return trans_id;
	}

	public void setTrans_id(String trans_id) {
		this.trans_id = trans_id;
	}

	public String getWsdl() {
		return wsdl;
	}

	public void setWsdl(String wsdl) {
		this.wsdl = wsdl;
	}

	public String getxApiKey() {
		return xApiKey;
	}

	public void setxApiKey(String xApiKey) {
		this.xApiKey = xApiKey;
	}

	@Override
	public String toString() {
		return "MmTransStatusRequestDto [login=" + login + ", trans_id=" + trans_id + ", wsdl=" + wsdl + ", xApiKey="
				+ xApiKey + "]";
	}

}
