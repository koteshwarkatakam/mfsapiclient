package com.mfs.client.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TransStatusResponseDto {

	private String mfs_trans_id;

	private Code code;

	private String message;

	private String statusMessage;

	public String getMfs_trans_id() {
		return mfs_trans_id;
	}

	public void setMfs_trans_id(String mfs_trans_id) {
		this.mfs_trans_id = mfs_trans_id;
	}

	public Code getCode() {
		return code;
	}

	public void setCode(Code code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	@Override
	public String toString() {
		return "TransStatusResponseDto [mfs_trans_id=" + mfs_trans_id + ", code=" + code + ", message=" + message
				+ ", statusMessage=" + statusMessage + "]";
	}

}
