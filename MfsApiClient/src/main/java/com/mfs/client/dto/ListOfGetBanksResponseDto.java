package com.mfs.client.dto;

import java.util.List;

import com.mfs.client.dto.GetBanksResponseDto;

public class ListOfGetBanksResponseDto {

	List<GetBanksResponseDto> getBanks;

	private String errorCode;
	private String errorMessage;

	public List<GetBanksResponseDto> getGetBanks() {
		return getBanks;
	}

	public void setGetBanks(List<GetBanksResponseDto> getBanks) {
		this.getBanks = getBanks;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString() {
		return "ListOfGetBanksResponseDto [getBanks=" + getBanks + ", errorCode=" + errorCode + ", errorMessage="
				+ errorMessage + "]";
	}

}
