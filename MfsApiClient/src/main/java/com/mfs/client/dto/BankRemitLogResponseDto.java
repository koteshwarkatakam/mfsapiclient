package com.mfs.client.dto;

public class BankRemitLogResponseDto {
	
	private ReturnBankRemitLog returnResponse;
	
	private String code;
	private String message;
	
	

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	

	public ReturnBankRemitLog getReturnResponse() {
		return returnResponse;
	}

	public void setReturnResponse(ReturnBankRemitLog returnResponse) {
		this.returnResponse = returnResponse;
	}

	@Override
	public String toString() {
		return "MmRemitLogResponseDto [returnResponse=" + returnResponse + ", code=" + code + ", message=" + message
				+ "]";
	}
	
	
	
	
    
    
    
}
