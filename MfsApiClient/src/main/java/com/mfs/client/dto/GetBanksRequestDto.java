package com.mfs.client.dto;

public class GetBanksRequestDto {
	
	private Login login;
	private String toCountry;
	private String wsdl;
	private String xApiKey;
	
	public Login getLogin() {
		return login;
	}
	public void setLogin(Login login) {
		this.login = login;
	}
	public String getToCountry() {
		return toCountry;
	}
	public void setToCountry(String toCountry) {
		this.toCountry = toCountry;
	}
	public String getWsdl() {
		return wsdl;
	}
	public void setWsdl(String wsdl) {
		this.wsdl = wsdl;
	}
	public String getxApiKey() {
		return xApiKey;
	}
	public void setxApiKey(String xApiKey) {
		this.xApiKey = xApiKey;
	}
	@Override
	public String toString() {
		return "GetBanksRequestDto [login=" + login + ", toCountry=" + toCountry + ", wsdl=" + wsdl + ", xApiKey="
				+ xApiKey + "]";
	}
	
	

}
