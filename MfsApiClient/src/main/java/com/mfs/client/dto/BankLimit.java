package com.mfs.client.dto;

public class BankLimit {
	
	private String max_daily_value;
	private String max_monthly_value;
	private String max_per_tx_limit;
	private String max_weekly_value; 
	private String min_per_tx_limit;
	
	public String getMax_daily_value() {
		return max_daily_value;
	}
	public void setMax_daily_value(String max_daily_value) {
		this.max_daily_value = max_daily_value;
	}
	public String getMax_monthly_value() {
		return max_monthly_value;
	}
	public void setMax_monthly_value(String max_monthly_value) {
		this.max_monthly_value = max_monthly_value;
	}
	public String getMax_per_tx_limit() {
		return max_per_tx_limit;
	}
	public void setMax_per_tx_limit(String max_per_tx_limit) {
		this.max_per_tx_limit = max_per_tx_limit;
	}
	public String getMax_weekly_value() {
		return max_weekly_value;
	}
	public void setMax_weekly_value(String max_weekly_value) {
		this.max_weekly_value = max_weekly_value;
	}
	public String getMin_per_tx_limit() {
		return min_per_tx_limit;
	}
	public void setMin_per_tx_limit(String min_per_tx_limit) {
		this.min_per_tx_limit = min_per_tx_limit;
	}
	@Override
	public String toString() {
		return "BankLimit [max_daily_value=" + max_daily_value + ", max_monthly_value=" + max_monthly_value
				+ ", max_per_tx_limit=" + max_per_tx_limit + ", max_weekly_value=" + max_weekly_value
				+ ", min_per_tx_limit=" + min_per_tx_limit + "]";
	}
	
	
	

}
