package com.mfs.client.dto;

public class Login {

	private String corporate_code;
	private String password;

	public String getCorporate_code() {
		return corporate_code;
	}

	public void setCorporate_code(String corporate_code) {
		this.corporate_code = corporate_code;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Login [corporate_code=" + corporate_code + ", password=" + password + "]";
	}

}