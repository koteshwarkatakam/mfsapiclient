package com.mfs.client.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ReturnDto {
	private String account_holder_name;
	private String account_number;
	private String mfs_bank_code;
	
	private String msisdn;
	private String partner_code;
	private Code status;
	
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getPartner_code() {
		return partner_code;
	}
	public void setPartner_code(String partner_code) {
		this.partner_code = partner_code;
	}
	public Code getStatus() {
		return status;
	}
	public void setStatus(Code status) {
		this.status = status;
	}
	public String getAccount_holder_name() {
		return account_holder_name;
	}
	public void setAccount_holder_name(String account_holder_name) {
		this.account_holder_name = account_holder_name;
	}
	public String getAccount_number() {
		return account_number;
	}
	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}
	public String getMfs_bank_code() {
		return mfs_bank_code;
	}
	public void setMfs_bank_code(String mfs_bank_code) {
		this.mfs_bank_code = mfs_bank_code;
	}
	@Override
	public String toString() {
		return "ReturnDto [account_holder_name=" + account_holder_name + ", account_number=" + account_number
				+ ", mfs_bank_code=" + mfs_bank_code + ", msisdn=" + msisdn + ", partner_code=" + partner_code
				+ ", status=" + status + "]";
	}
	
	
	
	
}
