package com.mfs.client.dto;

public class Sender {

	private String address;

	private String city;

	private String date_of_birth;

	private Document document;

	private String email;
	
	private String from_country;
	
	private String msisdn;
	
	private String name;
	
	private String postal_code;
	
	private String state;
	
	private String surname;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDate_of_birth() {
		return date_of_birth;
	}

	public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFrom_country() {
		return from_country;
	}

	public void setFrom_country(String from_country) {
		this.from_country = from_country;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPostal_code() {
		return postal_code;
	}

	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	
	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	@Override
	public String toString() {
		return "Sender [address=" + address + ", city=" + city + ", date_of_birth=" + date_of_birth + ", document="
				+ document + ", email=" + email + ", from_country=" + from_country + ", msisdn=" + msisdn + ", name="
				+ name + ", postal_code=" + postal_code + ", state=" + state + ", surname="
				+ surname + "]";
	}

	
	
}
