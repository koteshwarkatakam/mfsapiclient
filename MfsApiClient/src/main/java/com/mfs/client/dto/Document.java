package com.mfs.client.dto;

public class Document {
	

    private String id_country;

    private String id_expiry;
    private String id_number;
    private String id_type;

	public String getId_number() {
		return id_number;
	}

	public void setId_number(String id_number) {
		this.id_number = id_number;
	}

	public String getId_country() {
		return id_country;
	}

	public void setId_country(String id_country) {
		this.id_country = id_country;
	}

	public String getId_expiry() {
		return id_expiry;
	}

	public void setId_expiry(String id_expiry) {
		this.id_expiry = id_expiry;
	}

	public String getId_type() {
		return id_type;
	}

	public void setId_type(String id_type) {
		this.id_type = id_type;
	}

	@Override
	public String toString() {
		return "Document [id_number=" + id_number + ", id_country=" + id_country + ", id_expiry=" + id_expiry
				+ ", id_type=" + id_type + "]";
	}
    
}
