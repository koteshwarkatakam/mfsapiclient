package com.mfs.client.dto;

public class ReturnRemitLog {
	private String fx_rate;

	private String mfs_trans_id;

	private String name_match;

	private String partner_code;

	private Receive_amount receive_amount;

	private String sanction_list_recipient;

	private String sanction_list_sender;

	private String send_amount;
	

	private String third_party_trans_id;

	private Status status;

	public String getSanction_list_sender() {
		return sanction_list_sender;
	}

	public void setSanction_list_sender(String sanction_list_sender) {
		this.sanction_list_sender = sanction_list_sender;
	}

	public String getSend_amount() {
		return send_amount;
	}

	public void setSend_amount(String send_amount) {
		this.send_amount = send_amount;
	}

	public String getMfs_trans_id() {
		return mfs_trans_id;
	}

	public void setMfs_trans_id(String mfs_trans_id) {
		this.mfs_trans_id = mfs_trans_id;
	}

	public Receive_amount getReceive_amount() {
		return receive_amount;
	}

	public void setReceive_amount(Receive_amount receive_amount) {
		this.receive_amount = receive_amount;
	}

	public String getPartner_code() {
		return partner_code;
	}

	public void setPartner_code(String partner_code) {
		this.partner_code = partner_code;
	}

	public String getFx_rate() {
		return fx_rate;
	}

	public void setFx_rate(String fx_rate) {
		this.fx_rate = fx_rate;
	}

	public String getName_match() {
		return name_match;
	}

	public void setName_match(String name_match) {
		this.name_match = name_match;
	}

	public String getSanction_list_recipient() {
		return sanction_list_recipient;
	}

	public void setSanction_list_recipient(String sanction_list_recipient) {
		this.sanction_list_recipient = sanction_list_recipient;
	}

	public String getThird_party_trans_id() {
		return third_party_trans_id;
	}

	public void setThird_party_trans_id(String third_party_trans_id) {
		this.third_party_trans_id = third_party_trans_id;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Return [fx_rate=" + fx_rate + ", mfs_trans_id=" + mfs_trans_id + ", name_match=" + name_match
				+ ", partner_code=" + partner_code + ", receive_amount=" + receive_amount + ", sanction_list_recipient="
				+ sanction_list_recipient + ", sanction_list_sender=" + sanction_list_sender + ", send_amount="
				+ send_amount + ", third_party_trans_id=" + third_party_trans_id + ", status=" + status + "]";
	}

}
