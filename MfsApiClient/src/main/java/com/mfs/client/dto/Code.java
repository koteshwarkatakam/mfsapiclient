package com.mfs.client.dto;

public class Code {

	private String status_code;

	public String getStatus_code() {
		return status_code;
	}

	public void setStatus_code(String status_code) {
		this.status_code = status_code;
	}

	@Override
	public String toString() {
		return "Code [status_code=" + status_code + "]";
	}

}
