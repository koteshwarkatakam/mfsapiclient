package com.mfs.client.dto;

public class AccountRequestDto {

	private Login login;
	private String to_country;
	private String msisdn;	
	private String wsdl;
	private String xApiKey;
	
	public Login getLogin() {
		return login;
	}
	public void setLogin(Login login) {
		this.login = login;
	}
	public String getTo_country() {
		return to_country;
	}
	public void setTo_country(String to_country) {
		this.to_country = to_country;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getWsdl() {
		return wsdl;
	}
	public void setWsdl(String wsdl) {
		this.wsdl = wsdl;
	}
	public String getxApiKey() {
		return xApiKey;
	}
	public void setxApiKey(String xApiKey) {
		this.xApiKey = xApiKey;
	}
	
	@Override
	public String toString() {
		return "AccountRequestDto [login=" + login + ", to_country=" + to_country + ", msisdn=" + msisdn + ", wsdl="
				+ wsdl + ", xApiKey=" + xApiKey + "]";
	}
	
	
}
