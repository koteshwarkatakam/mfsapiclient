package com.mfs.client.dto;

public class Receive_amount {
	private Double amount;

    private String currency_code;

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getCurrency_code() {
		return currency_code;
	}

	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}

	@Override
	public String toString() {
		return "Receive_amount [amount=" + amount + ", currency_code=" + currency_code + "]";
	}

	

	

	

	
}
