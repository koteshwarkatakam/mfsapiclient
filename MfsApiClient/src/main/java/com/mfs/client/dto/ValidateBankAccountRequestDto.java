package com.mfs.client.dto;

public class ValidateBankAccountRequestDto {
	private Login login;
	private String to_country;
	private Payee payee;
	private Account account;
	private String wsdl;
	private String xApiKey;
	
	public Login getLogin() {
		return login;
	}
	public void setLogin(Login login) {
		this.login = login;
	}
	public String getTo_country() {
		return to_country;
	}
	public void setTo_country(String to_country) {
		this.to_country = to_country;
	}
	public Payee getPayee() {
		return payee;
	}
	public void setPayee(Payee payee) {
		this.payee = payee;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public String getWsdl() {
		return wsdl;
	}
	public void setWsdl(String wsdl) {
		this.wsdl = wsdl;
	}
	public String getxApiKey() {
		return xApiKey;
	}
	public void setxApiKey(String xApiKey) {
		this.xApiKey = xApiKey;
	}
	@Override
	public String toString() {
		return "AccountRequestDto [login=" + login + ", to_country=" + to_country + ", payee=" + payee + ", account="
				+ account + ", wsdl=" + wsdl + ", xApiKey=" + xApiKey + "]";
	}
	
	
}
