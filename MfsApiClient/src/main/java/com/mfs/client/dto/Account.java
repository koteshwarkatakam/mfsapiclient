package com.mfs.client.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Account {
	
	private String account_number;
	private String mfs_bank_code;
	
	public String getAccount_number() {
		return account_number;
	}
	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}
	public String getMfs_bank_code() {
		return mfs_bank_code;
	}
	public void setMfs_bank_code(String mfs_bank_code) {
		this.mfs_bank_code = mfs_bank_code;
	}
	
	@Override
	public String toString() {
		return "Account [account_number=" + account_number + ", mfs_bank_code=" + mfs_bank_code + "]";
	}
}
